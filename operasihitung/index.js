const { tambah, kurang, kali, bagi } = require('./operasihitung');

const a = 10;
const b = 5;

console.log(`Tambah: ${a} + ${b} = ${tambah(a, b)}`);
console.log(`Kurang: ${a} - ${b} = ${kurang(a, b)}`);
console.log(`Kali: ${a} * ${b} = ${kali(a, b)}`);
console.log(`Bagi: ${a} / ${b} = ${bagi(a, b)}`);
