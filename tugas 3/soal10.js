let people = [
    { name: "Dimas", age: 28 },
    { name: "Budi", age: 22 },
    { name: "Yanto", age: 32 }
];

let peopleAbove25 = people.filter(person => person.age > 25);
console.log(peopleAbove25);