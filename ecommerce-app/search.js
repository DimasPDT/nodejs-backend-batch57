const express = require('express');
const app = express();
const port = 3000;

// Data produk
let products = [
  { id: 1, name: 'Laptop', category: 'Elektronik', price: 1000 },
  { id: 2, name: 'Meja', category: 'Perabotan', price: 500 }
];

// Route dengan query string untuk mencari produk berdasarkan nama
app.get('/api/products/search', (req, res) => {
  const query = req.query.q;
  if (query) {
    const filteredProducts = products.filter(p => p.name.toLowerCase().includes(query.toLowerCase()));
    res.json(filteredProducts);
  } else {
    res.json(products);
  }
});

// Route dengan parameter dan query string untuk mendapatkan produk dalam kategori tertentu dan mencari berdasarkan nama
app.get('/api/products/category/:categoryName', (req, res) => {
  const categoryName = req.params.categoryName;
  const searchQuery = req.query.q;
  
  const filteredProducts = products.filter(p => 
    p.category.toLowerCase() === categoryName.toLowerCase() &&
    (!searchQuery || p.name.toLowerCase().includes(searchQuery.toLowerCase()))
  );

  res.json(filteredProducts);
});

// Route dengan query string
app.get('/api/search', (req, res) => {
  const query = req.query.q;
  // Lakukan pencarian berdasarkan query
  res.json({ query: query, results: ['Result 1', 'Result 2'] });
});

// Route dengan parameter dan query string
app.get('/api/users/:id/posts', (req, res) => {
  const userId = req.params.id;
  const searchQuery = req.query.q;
  // Ambil postingan pengguna berdasarkan ID dan query pencarian
  res.json({
    userId: userId,
    query: searchQuery,
    posts: ['Post 1', 'Post 2']
  });
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
