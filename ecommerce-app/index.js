var express = require("express");
var fileUpload = require("express-fileupload");
var cloudinary = require("./cloudinary");

var port = 3000;
var app = express();

app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: "/tmp/",
  })
);

app.post("/upload", (req, res) => {
  const files = req.files.photos;

  if (!files) {
    return res.status(400).json({ status: 400, message: "No files were uploaded." });
  }

  let uploadPromises = [];

  const filesArray = Array.isArray(files) ? files : [files];

  filesArray.forEach(file => {
    uploadPromises.push(
      cloudinary.v2.uploader.upload(file.tempFilePath, {
        public_id: new Date().getTime() + "_" + file.name,
      })
    );
  });

  Promise.all(uploadPromises)
    .then(results => {
      res.json({ status: 200, message: "Success", results });
    })
    .catch(error => {
      res.status(500).json({ status: 500, message: "Upload failed", error });
    });
});

// running apps
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
